;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gnu packages gcc-extra)
  #:use-module (srfi srfi-1)
  #:use-module (guix packages)
  #:use-module (gnu packages cross-base)
  #:use-module (gnu packages gcc))

(define-public gcc32
  (package
    (inherit (cross-gcc "i686-unknown-linux-gnu"
              #:libc (cross-libc "i686-unknown-linux-gnu")))
    (name "gcc32")
    (properties (alist-delete 'hidden? (package-properties gcc)))))
