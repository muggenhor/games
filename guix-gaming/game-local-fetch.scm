;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-gaming game-local-fetch)
  #:use-module (ice-9 match)
  #:use-module (guix download)
  #:use-module (guix build utils)
  #:use-module (guix-gaming utils)
  #:use-module (guix packages)
  #:export (game-local-fetch))

(define* (find-local-file-help name #:optional (message ""))
  (string-append
   (format #f "==> Cannot find local file ~s: ~a~%" name message)
   (format #f
           "Ensure that ~s is either in $GUIX_GAMING_PATH
(default to ~s)
or specified in ~s.

Example content for ~s:

  ((\"game-name\" . \"/path/to/file\")
   (\"my-other-game\" . \"...\"))~%"
           name guix-gaming-path-default
           (guix-gaming-channel-games-config)
           (guix-gaming-channel-games-config))))

(define (find-local-file name)
  "Find NAME on local system.
NAME path is first looked up in (guix-gaming-channel-games-config).
See `find-local-file-help' for more details.
If not found, NAME is looked up in GUIX_GAMING_PATH.
If several files match, return en error."
  (let* ((source (guix-gaming-channel-games-config))
         (path #f)
         (message #f))
    (when (access? source R_OK)
      (let ((config (call-with-input-file source read)))
        (set! path (assoc-ref config name))))
    (unless path
      (let* ((gaming-path (getenv* "GUIX_GAMING_PATH" guix-gaming-path-default))
             (results (find-files gaming-path name)))
        (match (length results)
          (0 (set! message (format #f "No match in ~a." gaming-path)))
          (1 (set! path (car results)))
          (l (set! message (format #f "Found too many matching files (~a) in ~a." l gaming-path))))))
    (or path
        (error (format #f "~a~%" (find-local-file-help name message))))))

(define* (game-local-fetch url hash-algo hash
                           #:optional name
                           #:key (system (%current-system))
                           (guile (default-guile)))
  "Return a fixed-output derivation that fetches URL (a string) which is
expected to have HASH of type HASH-ALGO (a symbol).  By default, the file name
is the base name of URL; optionally, NAME can specify a different file name.

This derivation will download URL and add the username and token as defined in
the configuration of the games channel."
  (define file-name (or name (basename url)))
  (url-fetch (find-local-file url)
             hash-algo hash file-name #:system system #:guile guile))
