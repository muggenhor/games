;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages the-dark-mod)
  #:use-module (ice-9 match)
  #:use-module (guix i18n)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system scons)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages xorg))

;; TODO: The Dark Mod is free software, so technically this package can be moved
;; to upstream Guix once the game data distribution is versioned.
(define-public the-dark-mod
  (let ((scons-flags
         (string-append "TARGET_ARCH="
                        (match (%current-system)
                          ("i686-linux" "x86")
                          ("x86_64-linux" "x64"))))
        (the-dark-mod-env-var-name "THEDARKMOD_PATH")
        (the-dark-mod-env-var-value "~/.local/share/darkmod"))
    (package
      (name "the-dark-mod")
      (version "2.07")
      (source (origin
                (method url-fetch)
                (uri (string-append "http://www.thedarkmod.com/sources/thedarkmod."
                                    version ".src.7z"))
                (sha256
                 (base32
                  ;; TODO: Hash seems unreliable, find a proper download source or report upstream.
                  "17wdpip8zvm2njz0xrf7xcxl73hnsc6i83zj18kn8rnjkpy50dd6"))))
      (build-system scons-build-system)
      (arguments
       `(#:tests? #f                    ;no test
         #:scons ,scons-python2
         ;; BUILD=release makes Scons strip the executable, which fails because
         ;; "strip" is not found in the path.
         #:scons-flags (list ,scons-flags)
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key source #:allow-other-keys)
               (and (invoke "7z" "x" source))))
           (add-after 'unpack 'fix-build-flags
             ;; TODO: By default, -no-pie is passed because of the assumption
             ;; that -fPIC was used to build ffmpeg.  This does not work with
             ;; out default gcc-5.  This package does not seem to build with
             ;; gcc>5.
             (lambda _
               (substitute* "SConstruct"
                 (("BASELINKFLAGS.append\\( '-no-pie' \\)") ""))
               #t))
           (add-after 'build 'build-updater
             (lambda _
               (with-directory-excursion "tdm_update"
                 (apply invoke "scons"
                        (append (list "-j" (number->string
                                            (parallel-job-count)))
                                (list ,scons-flags))))
               #t))
           (replace 'install
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (mesa (assoc-ref inputs "mesa"))
                      (bin (string-append out "/bin/thedarkmod"))
                      (bin-real (string-append out "/bin/.thedarkmod-real"))
                      (updater (string-append out "/bin/tdm_update"))
                      (updater-real (string-append out "/bin/.tdm_update-real"))
                      (apps (string-append out "/share/applications"))
                      (icons (string-append out "/share/icons")))
                 (mkdir-p (string-append out "/bin"))
                 (copy-file ,@(match (%current-system)
                                ("i686-linux"
                                 '("thedarkmod.x86"))
                                ("x86_64-linux"
                                 '("thedarkmod.x64")))
                            bin-real)
                 (copy-file "tdm_update/tdm_update.linux" updater-real)
                 (with-output-to-file bin
                   (lambda _
                     (format #t "\
#!~a
export LD_LIBRARY_PATH=~a/lib
~a=${~a:-~a}
cd \"$~a\"
exec -a \"~a\" ~a \"$@\"\n"
                             (which "bash")
                             mesa
                             ,the-dark-mod-env-var-name ,the-dark-mod-env-var-name
                             ,the-dark-mod-env-var-value
                             ,the-dark-mod-env-var-name
                             (basename bin) bin-real)))
                 (chmod bin #o555)
                 (with-output-to-file updater
                   (lambda _
                     (format #t "\
#!~a
~a=${~a:-~a}
mkdir -p \"$~a\"
## tdm_update outputs a log in its current working directory.
cd \"$~a\"
~a --noselfupdate --targetdir \"$~a\" \"$@\"~%"
                             (which "bash")
                             ,the-dark-mod-env-var-name ,the-dark-mod-env-var-name
                             ,the-dark-mod-env-var-value
                             ,the-dark-mod-env-var-name
                             ,the-dark-mod-env-var-name
                             updater-real
                             ,the-dark-mod-env-var-name)))
                 (chmod updater #o555)
                 (mkdir-p apps)
                 (mkdir-p icons)
                 (install-file "tdm_update/darkmod.ico" icons)
                 (with-output-to-file
                     (string-append apps "/darkmod.desktop")
                   (lambda _
                     (format #t
                             "[Desktop Entry]~@
                     Name=The Dark Mod~@
                     Comment=The Dark Mod~@
                     Exec=~a~@
                     TryExec=~@*~a~@
                     Icon=darkmod~@
                     Categories=Game~@
                     Type=Application~%"
                             bin)))))))))
      (inputs
       `(("mesa" ,mesa)
         ("libxxf86vm" ,libxxf86vm)
         ("openal" ,openal)
         ("libxext" ,libxext)))
      (native-inputs
       `(("p7zip" ,p7zip)
         ("m4" ,m4)
         ("subversion" ,subversion)))
      (home-page "http://www.thedarkmod.com/")
      (synopsis "Game based on the Thief series by Looking Glass Studios")
      (description (format #f (G_ "The Dark Mod (TDM) is stealth/infiltration game
based on the Thief series by Looking Glass Studios.  Formerly a Doom III mod,
it is now released as a standalone.

The game data must be fetched manually by running @command{tdm_update}.
The ~a environment variable specifies the location where the game data is
saved (defaults to ~a).")
                           the-dark-mod-env-var-name the-dark-mod-env-var-value))
      (supported-systems '("x86_64-linux" "i686-linux"))
      (license (list license:gpl3       ; idTech 4 engine
                     license:bsd-3 ; Portion of the engine by Broken Glass Studios
                     ;; All other non-software components: they are not
                     ;; included in the Guix package, but the updater fetches
                     ;; them.
                     license:cc-by-sa3.0)))))
