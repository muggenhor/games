;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages unigine)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix-gaming build-system binary)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg))

(define-public unigine-heaven
  (let ((browser (match (or (%current-target-system)
                            (%current-system))
                   ("x86_64-linux" "browser_x64")
                   ("i686-linux" "browser_x86")))
        (heaven (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "heaven_x64")
                  ("i686-linux" "heaven_x86")))
        (arch (match (or (%current-target-system)
                         (%current-system))
                ("x86_64-linux" "x64")
                ("i686-linux" "x86"))))
    (package
      (name "unigine-heaven")
      (version "4.0")
      (source
       (origin
         (method url-fetch)
         (uri (string-append "https://assets.unigine.com/d/Unigine_Heaven-"
                             version ".run"))
         (sha256
          (base32
           "19rndwwxnb9k2nw9h004hyrmr419471s0fp25yzvvc6rkd521c0v"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:strip-binaries? #f           ; Already stipped.
         #:patchelf-plan
         `((,,(string-append "bin/" browser)
            ("libc" "gcc" "out"))
           (,,(string-append "bin/" heaven)
            ("libc" "gcc" "out"))
           (,,(string-append "bin/libUnigine_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxext" "libxinerama" "libxrender"))
           (,,(string-append "bin/libAppStereo_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libAppSurround_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libAppWall_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libGPUMonitor_" arch ".so")
            ("libc" "gcc" "libx11" "libxext" "out"))
           (,,(string-append "bin/libQtCoreUnigine_" arch ".so.4")
            ("libc" "gcc" ))
           (,,(string-append "bin/libQtGuiUnigine_" arch ".so.4")
            ("libc" "gcc" "libxrender" "libxext" "libx11" "freetype"
             "fontconfig" "out"))
           (,,(string-append "bin/libQtNetworkUnigine_" arch ".so.4")
            ("libc" "gcc" "out"))
           (,,(string-append "bin/libQtWebKitUnigine_" arch ".so.4")
            ("libc" "gcc" "fontconfig" "freetype" "libxext" "libx11" "libxrender" "out"))
           (,,(string-append "bin/libQtXmlUnigine_" arch ".so.4")
            ("libc" "gcc" "out")))
         #:install-plan
         `(("bin" (,,(string-append "browser_" arch)
                   ,,(string-append "heaven_" arch))
            "share/unigine-heaven/bin/")
           ("bin" (,,(string-append "lib.*" arch ".*"))
            "lib/")
           ("data" (".*") "share/unigine-heaven/data/")
           ("documentation" (".*") "share/doc/unigine-heaven/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "bash")
                       (assoc-ref inputs "source"))
               (chdir (string-append "Unigine_Heaven-" ,version))
               #t))
           (add-after 'install 'wrap-program
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (share (string-append out "/share/unigine-heaven"))
                      (wrapper (string-append out "/bin/heaven")))
                 (mkdir-p (dirname wrapper))
                 (with-output-to-file wrapper
                   (lambda _
                     (format #t "#!~a/bin/bash~%" (which "bash"))
                     (format #t "export LD_LIBRARY_PATH=~a~%" (string-append
                                                               (assoc-ref inputs "openal") "/lib:"
                                                               (assoc-ref inputs "mesa") "/lib"))
                     (format #t "cd ~a~%" (string-append share "/bin"))
                     (format #t "~a/bin/browser_~a -config ~a/data/launcher/launcher.xml"
                             share ,arch share)))
                 (chmod wrapper #o755))
               #t)))))
      (inputs
       `(("gcc" ,(@@ (gnu packages gcc) gcc-9) "lib")
         ("libx11" ,libx11)
         ("libxrandr" ,libxrandr)
         ("libxext" ,libxext)
         ("libxinerama" ,libxinerama)
         ("libxrender" ,libxrender)
         ("freetype" ,freetype)
         ("fontconfig" ,fontconfig)
         ("mesa" ,mesa)
         ("openal" ,openal)))
      (home-page "https://benchmark.unigine.com/heaven")
      (synopsis "GPU-intensive benchmark focused on tessellation")
      (description "This benchmark immerses a user into a magical steampunk
world of shiny brass, wood and gears.  Nested on flying islands, a tiny village
with its cozy, sun-heated cobblestone streets, and a majestic dragon on the
central square gives a true sense of adventure.  An interactive experience with
fly-by and walk-through modes allows for exploring all corners of this world
powered by the UNIGINE Engine that leverages advanced capabilities of graphics
APIs and turns this bench into a visual masterpiece.")
      (license ((@@ (guix licenses) license)
                "Custom" "No URL?" "")))))

(define-public unigine-valley
  (let ((browser (match (or (%current-target-system)
                            (%current-system))
                   ("x86_64-linux" "browser_x64")
                   ("i686-linux" "browser_x86")))
        (engine (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "valley_x64")
                  ("i686-linux" "valley_x86")))
        (arch (match (or (%current-target-system)
                         (%current-system))
                ("x86_64-linux" "x64")
                ("i686-linux" "x86"))))
    (package
      (name "unigine-valley")
      (version "1.0")
      (source
       (origin
         (method url-fetch)
         (uri (string-append "https://assets.unigine.com/d/Unigine_Valley-"
                             version ".run"))
         (sha256
          (base32
           "1x306r24dxfa1lj9g5kq81x4xc8gqbqzbfxsh88ma60i8g98n32z"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:strip-binaries? #f           ; Already stipped.
         #:patchelf-plan
         `((,,(string-append "bin/" browser)
            ("libc" "gcc" "out"))
           (,,(string-append "bin/" engine)
            ("libc" "gcc" "out"))
           (,,(string-append "bin/libUnigine_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxext" "libxinerama" "libxrender"))
           (,,(string-append "bin/libAppStereo_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libAppSurround_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libAppWall_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libGPUMonitor_" arch ".so")
            ("libc" "gcc" "libx11" "libxext" "out"))
           (,,(string-append "bin/libQtCoreUnigine_" arch ".so.4")
            ("libc" "gcc" ))
           (,,(string-append "bin/libQtGuiUnigine_" arch ".so.4")
            ("libc" "gcc" "libxrender" "libxext" "libx11" "freetype"
             "fontconfig" "out"))
           (,,(string-append "bin/libQtNetworkUnigine_" arch ".so.4")
            ("libc" "gcc" "out"))
           (,,(string-append "bin/libQtWebKitUnigine_" arch ".so.4")
            ("libc" "gcc" "fontconfig" "freetype" "libxext" "libx11" "libxrender" "out"))
           (,,(string-append "bin/libQtXmlUnigine_" arch ".so.4")
            ("libc" "gcc" "out")))
         #:install-plan
         `(("bin" (,,(string-append "browser_" arch)
                   ,,(string-append "valley_" arch))
            "share/unigine-valley/bin/")
           ("bin" (,,(string-append "lib.*" arch ".*"))
            "lib/")
           ("data" (".*") "share/unigine-valley/data/")
           ("documentation" (".*") "share/doc/unigine-valley/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "bash")
                       (assoc-ref inputs "source"))
               (chdir (string-append "Unigine_Valley-" ,version))
               #t))
           (add-after 'install 'wrap-program
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (share (string-append out "/share/unigine-valley"))
                      (wrapper (string-append out "/bin/valley")))
                 (mkdir-p (dirname wrapper))
                 (with-output-to-file wrapper
                   (lambda _
                     (format #t "#!~a/bin/bash~%" (which "bash"))
                     (format #t "export LD_LIBRARY_PATH=~a~%" (string-append
                                                               (assoc-ref inputs "openal") "/lib:"
                                                               (assoc-ref inputs "mesa") "/lib"))
                     (format #t "cd ~a~%" (string-append share "/bin"))
                     (format #t "~a/bin/browser_~a -config ~a/data/launcher/launcher.xml"
                             share ,arch share)))
                 (chmod wrapper #o755))
               #t)))))
      (inputs
       `(("gcc" ,(@@ (gnu packages gcc) gcc-9) "lib")
         ("libx11" ,libx11)
         ("libxrandr" ,libxrandr)
         ("libxext" ,libxext)
         ("libxinerama" ,libxinerama)
         ("libxrender" ,libxrender)
         ("freetype" ,freetype)
         ("fontconfig" ,fontconfig)
         ("mesa" ,mesa)
         ("openal" ,openal)))
      (home-page "https://benchmark.unigine.com/valley")
      (synopsis "GPU-intensive benchmark focused on vegetation")
      (description "The forest-covered valley surrounded by vast mountains
amazes with its scale from a bird’s-eye view and is extremely detailed down to
every leaf and flower petal.  Valley Benchmark allows you to encounter a morning
high up in the mountains when the snow-capped peaks are just barely glittering
in the rising sun.  Be it flying over the vast green expanses or hiking along
rocky slopes, this journey continues as long as you wish.  Unique in every
corner, this open-space world provides a wonderfully relaxing experience under
the tranquil music and sounds of nature.")
      (license ((@@ (guix licenses) license)
                "Custom" "No URL?" "")))))
