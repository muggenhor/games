;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages fez)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix-gaming build-system binary)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages mono)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xorg)
  #:use-module (guix-gaming utils)
  #:use-module (guix-gaming build utils)
  #:use-module (guix-gaming humble-bundle))

(define-public fez
  (let* ((version "11282016")           ; Actually 1.12.
         (file-name (string-append "fez-" version "-bin"))
         (binary (match (or (%current-target-system)
                            (%current-system))
                   ("x86_64-linux" "FEZ.bin.x86_64")
                   ("i686-linux" "FEZ.bin.x86")
                   (_ ""))))
    (package
      (name "fez")
      (version "1.12")
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "1232jcalp1b4lcpgga1xy3xif23i1549q2j0s66hmq2ymf29z4nc"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:strip-binaries? #f       ; allocated section `.dynstr' not in segment
         #:patchelf-plan
         `((,,(string-append "data/" binary)))
         #:install-plan
         `(;; Seems that we cannot rename the binary file, or else we get "File
           ;; does not contain a valid CIL image."
           ("data" (,,binary) "share/fez/")
           ("data" ("FEZ.exe") "share/fez/") ; We need the Windows Mono binary.
           ("data" ("Changelog.txt" "README.txt" "Linux.README") "share/doc/fez/")
           ("data" ("FEZ.bmp") "share/pixmaps/")
           ("data" ("FNA.dll.config" "gamecontrollerdb.txt" "monoconfig") "share/fez/")
           ("data" (".*.dll$") "share/fez/")           ; TODO: Can we reuse Mono's .dlls (e.g. mscorlib.dll)?  Also try packaging https://github.com/FNA-XNA/FNA.
           ("data/Content" (".*") "share/fez/Content/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               ;; We use system* to ignore errors.
               (system* (which "unzip")
                        (assoc-ref inputs "source"))
               #t))
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/fez"))
                      (real (string-append output "/share/fez/" ,binary)))
                 (mkdir-p (dirname wrapper))
                 (call-with-output-file wrapper
                   (lambda (p)
                     (format p
                             (string-append
                              "#!" (assoc-ref inputs "bash") "/bin/bash" "\n"
                              "cd " output "/share/fez" "\n"
                              "export LD_LIBRARY_PATH="
                              (string-join
                               (map (lambda (lib) (string-append (assoc-ref inputs lib) "/lib"))
                                    '("sdl2" "mono" "libvorbis" "libogg" "openal" "mojoshader"))
                               ":")
                              "\n"
                              "exec -a " (basename wrapper) " " real " \"$@\"" "\n"))))
                 (chmod wrapper #o755)
                 (make-desktop-entry-file (string-append output "/share/applications/fez.desktop")
                                          #:name "FEZ"
                                          #:exec wrapper
                                          #:icon (string-append output "/share/pixmaps/FEZ.bmp")
                                          #:comment "Gomez is a 2D creature living in a 2D world. Or is he?"
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("sdl2" ,sdl2)
         ("mojoshader" ,mojoshader-with-viewport-flip)
         ("mono" ,mono)
         ("libvorbis" ,libvorbis)
         ("libogg" ,libogg)
         ("openal" ,openal)))
      (home-page "http://fezgame.com/")
      (synopsis "Puzzle-platform video game")
      (description "The player-character Gomez receives a fez that reveals his
two-dimensional (2D) world to be one of four sides of a three-dimensional (3D)
world.  The player rotates between these four 2D views to realign platforms and
solve the game's puzzles.  The object of the game is to collect cubes and cube
fragments to restore order to the universe.")
      (license ((@@ (guix licenses) license)
                "Fez End User License Agreement"
                "No URL?" "")))))
